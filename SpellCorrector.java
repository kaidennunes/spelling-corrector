/**
 * Created by Kaiden on 1/16/2018.
 */
package spell;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map;
import java.util.List;
import java.util.Vector;


public class SpellCorrector implements ISpellCorrector {

    private Trie trieDictionary = new Trie();

    // First arg is dictionary file, second arg is word to be checked
    public static void main(String[] args) throws IOException {

        // Put the arguments into Strings
        String dictionaryFileName = args[0];
        String inputWord = args[1];

        // Create instance of  Spell Corrector interface
        ISpellCorrector corrector = new SpellCorrector();

        // Add dictionary to the Spell Corrector
        corrector.useDictionary(dictionaryFileName);

        // Get the most probable word
        String suggestion = corrector.suggestSimilarWord(inputWord);

        // If we couldn't find any word, tell the user that, otherwise, print out that word.
        if (suggestion == null) {
            suggestion = "No similar word found";
        }
        System.out.println("Suggestion is: " + suggestion);
    }

    public void useDictionary(String dictionaryFileName) throws IOException {
        StringBuilder content = new StringBuilder();
        File file = new File(dictionaryFileName);
        try (Scanner scan = new Scanner(file)) {
            while (scan.hasNextLine()) {
                content.append(" ");
                content.append(scan.nextLine());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Could not find file: " + file);
            ex.printStackTrace();
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal argument: Fix your bug!");
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        }
        String[] splitArray = content.toString().trim().split("\\s+");
        for (String word : splitArray) {
            trieDictionary.add(lowerCase(word));
        }
    }

    public String suggestSimilarWord(String inputWord) {
        // First see if it is the actual word
        if (trieDictionary.find(inputWord) != null) {
            return inputWord;
        }

        // Create a map to hold my potential word value of the form: <word, frequency>
        Map<String, Integer> matchedWords = new TreeMap<String, Integer>();

        // Fill the map full of suggestions and use it to find the best word
        return suggestSimilarWordHelper(matchedWords, inputWord);
    }

    private String suggestSimilarWordHelper(Map<String, Integer> matchedWords, String inputWord) {
        List<String> listOfOnceEditedWords = new Vector<String>();

        // Start with deletion
        deletionEdit(matchedWords, inputWord, listOfOnceEditedWords);

        // Then use transposition
        transpositionEdit(matchedWords, inputWord, listOfOnceEditedWords);

        // Then use alternation
        alternationEdit(matchedWords, inputWord, listOfOnceEditedWords);

        // Then use insertion
        insertionEdit(matchedWords, inputWord, listOfOnceEditedWords);

        // See if we found any decent matches
        String bestWord = getBestStringOfBestNode(matchedWords);

        // If we didn't find anything, try again.
        if (bestWord == null) {
            // Try again for each word we created from the edits.
            for (String element : listOfOnceEditedWords) {

                // Start with deletion
                deletionEdit(matchedWords, element);

                // Then use transposition
                transpositionEdit(matchedWords, element);

                // Then use alternation
                alternationEdit(matchedWords, element);

                // Then use insertion
                insertionEdit(matchedWords, element);
            }

            return getBestStringOfBestNode(matchedWords);
        }

        // Otherwise, return that word
        return bestWord;
    }

    private static String getBestStringOfBestNode(Map<String, Integer> matchedWords) {
        StringBuilder bestMatch = new StringBuilder();
        int frequency = 0;
        for (Map.Entry<String, Integer> entry : matchedWords.entrySet())
        {
            // If this entry doesn't have a higher word count, ignore it.
            if (entry.getValue() <= frequency) {
                continue;
            }
            // Set the better match values
            frequency = entry.getValue();
            bestMatch.setLength(0);
            bestMatch.append(entry.getKey());
        }

        // If we have no value, return null
        if (bestMatch == null || bestMatch.toString().equals("")) {
            return null;
        }

        // Return the best string
        return bestMatch.toString();
    }

    private void deletionEdit(Map<String, Integer> matchedWords, String inputWord, List<String> listOfOnceEditedWords) {
        for (int i = 0; i< inputWord.length();i++) {
            String newWord = deleteCharAt(inputWord, i);

            // Find the node that represents that string.
            ITrie.INode node = trieDictionary.find(newWord);

            // If that word exists, find its frequency and add it to the dictionary
            if (node != null) {
                matchedWords.put(newWord, node.getValue());
            }

            // Add that word to the list of edited words
            listOfOnceEditedWords.add(newWord);
        }
    }

    private void transpositionEdit(Map<String, Integer> matchedWords, String inputWord, List<String> listOfOnceEditedWords) {
        for (int i = 0; i< inputWord.length() - 1;i++) {
            char firstTransposedCharacter = inputWord.charAt(i);
            char secondTransposedCharacter = inputWord.charAt(i + 1);

            StringBuilder newWord = new StringBuilder(inputWord);

            newWord.setCharAt(i, secondTransposedCharacter);
            newWord.setCharAt(i + 1, firstTransposedCharacter);

            // Find the node that represents that string.
            ITrie.INode node = trieDictionary.find(newWord.toString());

            // If that word exists, find its frequency and add it to the dictionary
            if (node != null) {
                matchedWords.put(newWord.toString(), node.getValue());
            }

            // Add that word to the list of edited words
            listOfOnceEditedWords.add(newWord.toString());
        }
    }

    private void alternationEdit(Map<String, Integer> matchedWords, String inputWord, List<String> listOfOnceEditedWords) {
        for (int i = 0; i< inputWord.length();i++) {
            for (int j = 0; j< 26;j++) {
                StringBuilder newWord = new StringBuilder(inputWord);
                char alternateCharacter = (char) (j + 'a');
                newWord.setCharAt(i, alternateCharacter);

                // Find the node that represents that string.
                ITrie.INode node = trieDictionary.find(newWord.toString());

                // If that word exists, find its frequency and add it to the dictionary
                if (node != null) {
                    matchedWords.put(newWord.toString(), node.getValue());
                }

                // Add that word to the list of edited words
                listOfOnceEditedWords.add(newWord.toString());
            }
        }
    }

    private void insertionEdit(Map<String, Integer> matchedWords, String inputWord, List<String> listOfOnceEditedWords) {
        for (int i = 0; i<= inputWord.length();i++) {
            for (int j = 0; j< 26;j++) {
                StringBuilder newWord = new StringBuilder(inputWord);

                char insertCharacter = (char)(j + 'a');
                newWord.insert(i, insertCharacter);

                // Find the node that represents that string.
                ITrie.INode node = trieDictionary.find(newWord.toString());

                // If that word exists, find its frequency and add it to the dictionary
                if (node != null) {
                    matchedWords.put(newWord.toString(), node.getValue());
                }

                // Add that word to the list of edited words
                listOfOnceEditedWords.add(newWord.toString());
            }
        }
    }
    private void deletionEdit(Map<String, Integer> matchedWords, String inputWord) {
        for (int i = 0; i< inputWord.length();i++) {
            String newWord = deleteCharAt(inputWord, i);

            // Find the node that represents that string.
            ITrie.INode node = trieDictionary.find(newWord);

            // If that word exists, find its frequency and add it to the dictionary
            if (node != null) {
                matchedWords.put(newWord, node.getValue());
            }
        }
    }

    private void transpositionEdit(Map<String, Integer> matchedWords, String inputWord) {
        for (int i = 0; i< inputWord.length() - 1;i++) {
            char firstTransposedCharacter = inputWord.charAt(i);
            char secondTransposedCharacter = inputWord.charAt(i + 1);

            StringBuilder newWord = new StringBuilder(inputWord);

            newWord.setCharAt(i, secondTransposedCharacter);
            newWord.setCharAt(i + 1, firstTransposedCharacter);

            // Find the node that represents that string.
            ITrie.INode node = trieDictionary.find(newWord.toString());

            // If that word exists, find its frequency and add it to the dictionary
            if (node != null) {
                matchedWords.put(newWord.toString(), node.getValue());
            }
        }
    }

    private void alternationEdit(Map<String, Integer> matchedWords, String inputWord) {
        for (int i = 0; i< inputWord.length();i++) {
            for (int j = 0; j< 26;j++) {
                StringBuilder newWord = new StringBuilder(inputWord);
                char alternateCharacter = (char) (j + 'a');
                newWord.setCharAt(i, alternateCharacter);

                // Find the node that represents that string.
                ITrie.INode node = trieDictionary.find(newWord.toString());

                // If that word exists, find its frequency and add it to the dictionary
                if (node != null) {
                    matchedWords.put(newWord.toString(), node.getValue());
                }
            }
        }
    }

    private void insertionEdit(Map<String, Integer> matchedWords, String inputWord) {
        for (int i = 0; i<= inputWord.length();i++) {
            for (int j = 0; j< 26;j++) {
                StringBuilder newWord = new StringBuilder(inputWord);

                char insertCharacter = (char)(j + 'a');
                newWord.insert(i, insertCharacter);

                // Find the node that represents that string.
                ITrie.INode node = trieDictionary.find(newWord.toString());

                // If that word exists, find its frequency and add it to the dictionary
                if (node != null) {
                    matchedWords.put(newWord.toString(), node.getValue());
                }
            }
        }
    }

    private static String deleteCharAt(String strValue, int index) {
        return strValue.substring(0, index) + strValue.substring(index + 1);
    }

    private static String lowerCase (String data)
    {
        StringBuilder lowerCaseString = new StringBuilder();
        lowerCaseString.append(data.substring(0).toLowerCase());
        return lowerCaseString.toString();
    }
}

/**
 * Created by Kaiden on 1/16/2018.
 */
package spell;

public class Trie implements ITrie {

    private TrieNode root = new TrieNode();

    private int wordCount = 0;
    private int nodeCount = 0;

    public void Trie() {

    }

    public void add(String word) {
        wordCount++;
        StringBuilder newWord = new StringBuilder();
        root.addWord(newWord.append(word));
    }

    public INode find(String word) {
        if (word.equals("")) {
            return null;
        }
        StringBuilder wordToFind = new StringBuilder();
        wordToFind.append(word);
        return findHelper(root, wordToFind);
    }

    private INode findHelper(TrieNode node, StringBuilder word) {
        if (word.toString().equals("") && node.getValue() > 0) {
            return node;
        }

        if (word.toString().equals("")) {
            return null;
        }

        int index = word.charAt(0) - 'a';
        word.deleteCharAt(0);

        if (node.trieChildrenNodes == null) {
            return null;
        }
        
        if (node.trieChildrenNodes[index] == null) {
            return null;
        }
        return findHelper(node.trieChildrenNodes[index], word);
    }

    /**
     * Returns the number of unique words in the trie
     *
     * @return The number of unique words in the trie
     */
    public int getWordCount() {
        return wordCount;
    }

    /**
     * Returns the number of nodes in the trie
     *
     * @return The number of nodes in the trie
     */
    public int getNodeCount() {
        return nodeCount;
    }

    /**
     * The toString specification is as follows:
     * For each word, in alphabetical order:
     * <word>\n
     */
    @Override
    public String toString() {
        StringBuilder word = new StringBuilder();
        StringBuilder output = new StringBuilder();
        toStringHelper(root, word, output);
        return output.toString();
    }

    @Override
    public int hashCode() {
        return wordCount * nodeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Trie comparedTrie = (Trie) o;

        if (this.getNodeCount() != comparedTrie.getNodeCount()) {
            return false;
        }

        if (this.getWordCount() != comparedTrie.getWordCount()) {
            return false;
        }

        return equalsHelper(root, comparedTrie.root);
    }

    private boolean equalsHelper(TrieNode originalNode, TrieNode comparedNode){
        if (originalNode.getValue() != comparedNode.getValue()) {
            return false;
        }

        if (originalNode.trieChildrenNodes == null && comparedNode.trieChildrenNodes != null) {
            return false;
        }

        if (originalNode.trieChildrenNodes != null && comparedNode.trieChildrenNodes == null) {
            return false;
        }

        if (originalNode.trieChildrenNodes == null) {
            return true;
        }

        for (int i = 0;i<26;i++) {
            if (originalNode.trieChildrenNodes[i] == null && comparedNode.trieChildrenNodes[i] != null) {
                return false;
            }
            if (originalNode.trieChildrenNodes[i] != null && comparedNode.trieChildrenNodes[i] == null) {
                return false;
            }
            if (originalNode.trieChildrenNodes[i] != null) {
                if(!equalsHelper(originalNode.trieChildrenNodes[i], comparedNode.trieChildrenNodes[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    private void toStringHelper(TrieNode node, StringBuilder word, StringBuilder output) {
        // If this node is a word, then add it to the output
        if (node.getValue() > 0) {
            output.append(word.toString());
            output.append("\n");
        }

        if (node.trieChildrenNodes == null) {
            return;
        }
        // Go through all of its children in order
        for (int i = 0; i < 26; i++) {
            // If that child is null, ignore it, move on
            if (node.trieChildrenNodes[i] == null) {
                continue;
            }

            // Otherwise, add this node's value to the word
            char nodeValue = (char) (i + 'a');
            word.append(nodeValue);

            // Call recursively the same method on that child
            toStringHelper(node.trieChildrenNodes[i], word, output);

            // Remove this node's value to the word
            word.deleteCharAt(word.length() - 1);
        }
    }

    public class TrieNode implements INode {

        private TrieNode[] trieChildrenNodes;

        int value = 0;

        public TrieNode() {

        }

        public void incrementValue() {
            this.value++;
        }

        public int getValue() {
            return this.value;
        }

        public void addWord(StringBuilder word) {

            // If there is nothing left in the StringBuilder, we have already reached the end
            if (word == null || word.toString().equals("")) {
                incrementValue();
                return;
            }

            // If we haven't yet initialized the children, do so.
            if (trieChildrenNodes == null) {
                trieChildrenNodes = new TrieNode[26];
            }

            // Get proper index
            int index = word.charAt(0) - 'a';

            // Delete that character from the StringBuilder
            word.deleteCharAt(0);

            // If there is no node initialized at that index, initialize it.
            if (trieChildrenNodes[index] == null) {
                trieChildrenNodes[index] = new TrieNode();
                nodeCount++;
            }

            // Continue to add the characters to the data structure
            trieChildrenNodes[index].addWord(word);
        }
    }
}
